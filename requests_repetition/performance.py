import requests
from requests.exceptions import Timeout

print(requests.get('https://api.github.com', timeout=1))
print(requests.get('https://api.github.com', timeout=3.05))
print(requests.get('https://api.github.com', timeout=(2, 5)))


try:
    response = requests.get('https://api.github.com', timeout=1)
except Timeout:
    print('The request timed out')
else:
    print('The request did not time out')