import requests
from getpass import getpass

# with requests.Session() as session:
#     session.auth = ("andrzejstopka", getpass())

#     response = session.get('https://api.github.com/user')

# print(response.headers)
# print(response.json())


#######################

from requests.adapters import HTTPAdapter
from requests.exceptions import ConnectionError

github_adapter = HTTPAdapter(max_retries=3)

session = requests.Session()

session.mount('https://api.github.com', github_adapter)


try:
    print(session.get('https://api.github.com'))
except ConnectionError as ce:
    print(ce)

