from getpass import getpass
import requests

# print(requests.get('https://api.github.com/user', auth=('andrzejstopka', getpass())))



from requests.auth import HTTPBasicAuth

# print(requests.get('https://api.github.com/user', auth=HTTPBasicAuth('andrzejstopka', getpass())))


from requests.auth import AuthBase

class TokenAuth(AuthBase):
    def __init__(self, token):
        self.token = token

    def __call__(self, r):
        r.headers["X-TokenAuth"] = f"{self.token}"
        return r
    
# print(requests.get('https://httpbin.org/get', auth=TokenAuth('12345abcde-token')))


print(requests.get('https://api.github.com', verify=False))



