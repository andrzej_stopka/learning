import unittest
from account import Account
from unittest.mock import MagicMock


class TestAccount(unittest.TestCase):
    def setUp(self):
        self.account = Account(100)

    def test_withdraw_has_balance(self):
        withdrawal_amount = 50

        self.account.withdraw(withdrawal_amount)

        self.assertEqual(self.account.get_balance(), 50)

    def test_withdraw_not_enough_balance(self):
        withdrawal_amount = 400

        self.account.withdraw(withdrawal_amount)

        self.assertEqual(self.account.get_balance(), 100)

    def test_withdraw_has_balance_stab(self):
        withdrawal_amount = 40

        def stub():
            return 60
        
        original_get_balance = self.account.get_balance

        try:
            self.account.get_balance = stub

            self.account.withdraw(withdrawal_amount)

            self.assertEqual(self.account.get_balance(), 60)
        
        finally:
            self.account.get_balance = original_get_balance

    def test_withdraw_has_balance_mock(self):
        withdrawal_amount = 75
        self.account.update_balance = MagicMock()

        self.account.withdraw(withdrawal_amount)

        self.account.update_balance.assert_called_with(25)

if __name__ == '__main__':
    unittest.main()