class Account():
    __balance = 0

    def __init__(self, initial_balance):
        self.__balance = initial_balance

    def get_balance(self):
        return self.__balance

    def update_balance(self, amount):
        self.__balance = amount

    def withdraw(self, amount):
        if self.__balance >= amount:
            self.update_balance(self.__balance - amount)
    

