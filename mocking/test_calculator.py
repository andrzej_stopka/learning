
from unittest import TestCase
from unittest.mock import patch


def mock_sum(a, b):
    return a + b


class TestCalculator(TestCase):
    @patch('calculator.Calculator.sum', side_effect=mock_sum)

    def test_sum(self, sum):
        self.assertEqual(sum(2,3), 5)
        self.assertEqual(sum(7,3), 10)


