import blog_class

from unittest import TestCase
from unittest.mock import patch

class TestBlog(TestCase):
    @patch ('blog_class.Blog')
    def test_blog_posts(self, MockBlog):
        blog = MockBlog()

        blog.posts.return_value = [
            {
                'userId': 1,
                'id': 1,
                'title': 'Test Title',
                'body': 'Far out in the uncharted backwaters of the unfashionable  end  of the  western  spiral  arm  of  the Galaxy\ lies a small unregarded yellow sun.'
            }
        ]

        response = blog.posts()
        self.assertIsNotNone(response)
        self.assertIsInstance(response[0], dict)

        assert MockBlog is blog_class.Blog
        assert MockBlog.called
        blog.posts.assert_called_with()
        blog.posts.assert_called_once_with()
        # blog.posts.assert_called_with(1, 2, 3)
        blog.reset_mock()
        blog.posts.assert_not_called()